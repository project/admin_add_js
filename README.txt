CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The module Admin add_js allows the admin (with proper permissions) to add the script via admin configuration.

There is a configuration option for the Admin to enter the script.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/admin_add_js

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/admin_add_js


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Admin add_js module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/docs/7/extend/installing-modules for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Modules and enable the module.
    2. Navigate to admin/config/admin_add_js for configurations.
    3. Script can be entered and submit.


MAINTAINERS
-----------

Current maintainers:
 * Siva Iyyappan - https://www.drupal.org/u/siva_drupal

Supporting organizations:
 * Zyxware Technologies - https://www.drupal.org/zyxware-technologies
